package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.config.JwtToken;
import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import com.zuitt.wdc044_s01.repositories.PostRepository;
import com.zuitt.wdc044_s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    // it automatically injects dependencies
    // it saves you from manually creating and wiring dependencies allowing Spring to handle to process for you
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(post.getUser());
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }
}

package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
// to indicate that a particular class is responsible for handling incoming HTTP requests and generating appropriate responses
@CrossOrigin
// to enable Cross Origin Resource Sharing
// allows data sharing between different domains
// Example: When a client (browser) makes a request to a server, the server typically restricts access to its resources originating from other origin (different domain, protocol and port)
// There are cases where you might want to allow request from a different origin. This is where CORS come into play or @CrossOrigin

public class PostController {
    @Autowired
    PostService postService;

    // Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost (@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Retrieve all posts
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Post>> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }
}

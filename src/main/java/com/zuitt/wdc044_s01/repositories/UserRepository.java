package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
// custom method for finding a user by their username
public interface UserRepository extends CrudRepository <User, Object> {
    User findByUsername(String username);
}

// This will be used in creating JWT (JwtToken class)
